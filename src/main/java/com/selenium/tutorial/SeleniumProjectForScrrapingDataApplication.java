package com.selenium.tutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeleniumProjectForScrrapingDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeleniumProjectForScrrapingDataApplication.class, args);
	}

}

package com.selenium.tutorial.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.compress.utils.Charsets;
import org.checkerframework.common.reflection.qual.GetClass;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.io.Resources;
import com.paulhammant.ngwebdriver.NgWebDriver;

@RestController
@RequestMapping("/Selenium")
public class MandiriRestApi {

	@GetMapping("/Mandiri/Api/Web")
	public String mandiriWebApi() throws InterruptedException, IOException {
//		Initiation
		System.setProperty("webdriver.chrome.driver",
				"C:\\CashlezProject\\Selenium Java and Driver\\chromedriver\\chromedriver.exe");

//		Selenium Without Browser
		ChromeOptions opt = new ChromeOptions();
		opt.addArguments("--headless", "--window-size=1920,1200", "--ignore-certificate-errors");
		opt.addArguments("start-maximized");
		opt.addArguments("disable-infobars");
		opt.addArguments("--disable-extensions");		
		opt.addArguments("--user-agent = Googlebot/2.1 (+http://www.google.com/bot.html)");
		opt.addArguments("download.default_directory",
				System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator + "downloadFiles");
		
//		String downloadFilepath = "/path/to/download";
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
//		chromePrefs.put("download.default_directory", downloadFilepath);
		opt.setExperimentalOption("prefs", chromePrefs);
		opt.addArguments("disable-infobars");
		opt.setAcceptInsecureCerts(true);
		opt.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
		opt.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		opt.setCapability(ChromeOptions.CAPABILITY, opt);
		WebDriver drivers = new ChromeDriver(opt);

		JavascriptExecutor jss = (JavascriptExecutor) drivers;
		NgWebDriver ngDriver = new NgWebDriver(jss);
		try {
//		        Login Code
			drivers.get("https://mib.bankmandiri.co.id/sme/common/login.do?action=logoutSME");
//			drivers.manage().window().maximize();
			ngDriver.waitForAngularRequestsToFinish();
			System.out.println(drivers.getCurrentUrl());
			WebElement loginCompanyId = drivers.findElement(By.xpath(
					"/html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[2]/input"));
			loginCompanyId.sendKeys("FA23426");
			WebElement loginUsername = drivers.findElement(By.xpath(
					"/html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/input"));
			loginUsername.sendKeys("MAKER01");
			WebElement loginPasssword = drivers.findElement(By.xpath(
					"/html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td[2]/input[2]"));
			loginPasssword.sendKeys("Cashlez0");
			WebElement loginButton = drivers.findElement(By.xpath("//*[@id=\"button\"]"));
			loginButton.sendKeys(Keys.ENTER);
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(1000);

//			Go To Menu
//			System.out.println(drivers.switchTo().alert().getText());
			drivers.switchTo().frame("menuFrame");
//			Transfer Button
			WebElement transferButton = drivers.findElement(By.xpath("//*[@id=\"masterdiv\"]/div[2]/a"));
			transferButton.click();
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			WebElement mutasiRekeningButton = drivers.findElement(By.xpath("//*[@id=\"subs9\"]"));
			mutasiRekeningButton.click();
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			drivers.switchTo().defaultContent();
			drivers.switchTo().frame("mainFrame");
			Select select = new Select(drivers.findElement(By.xpath("/html/body/form/table[3]/tbody/tr[7]/td[2]/select")));
			select.selectByValue("CSV");
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
//			System.out.println(drivers.getPageSource());
////			Add Calendar
			WebElement changeDateButtonAwalDay = drivers.findElement(By.xpath("/html/body/form/table[3]/tbody/tr[1]/td[2]/input[1]"));
			WebElement changeDateButtonAwalMonth = drivers.findElement(By.xpath("/html/body/form/table[3]/tbody/tr[1]/td[2]/input[2]"));
			WebElement changeDateButtonAwalYear = drivers.findElement(By.xpath("/html/body/form/table[3]/tbody/tr[1]/td[2]/input[3]"));			
			
			JavascriptExecutor jsExecutor = (JavascriptExecutor) drivers;  			
			//set the text
			jsExecutor.executeScript("arguments[0].value='01'", changeDateButtonAwalDay);  
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(3000);
			jsExecutor.executeScript("arguments[0].value='12'", changeDateButtonAwalMonth);
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(3000);
			jsExecutor.executeScript("arguments[0].value='2022'", changeDateButtonAwalYear);
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(3000);
//			Download Button
			WebElement downloadButton = drivers.findElement(By.xpath("/html/body/form/table[4]/tbody/tr/td/input[3]"));
			downloadButton.click();
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			
//			Get Tetxt Success Download
			WebElement noImportantButton = drivers
					.findElement(By.xpath("/html/body/form/table[3]/tbody/tr[1]/td/b/font/li"));
			String response = noImportantButton.getText().toString();
//			String response = "OK";
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(1000);

//			Unduh Laporan
			System.out.println("Download Laporan");
			drivers.switchTo().defaultContent();
			drivers.switchTo().frame("menuFrame");
			System.out.println("Menu Unduh Laporan 1");
			WebElement downloadStatementButton1 = drivers.findElement(By.xpath("//*[@id=\"masterdiv\"]/div[4]/a"));
			downloadStatementButton1.click();
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(1000); 
			System.out.println("Menu Unduh Laporan 2");
			WebElement downloadStatementButton2 = drivers.findElement(By.xpath("//*[@id=\"subs31\"]"));
			downloadStatementButton2.click();
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(1000);
			System.out.println("Menu Unduh Laporan Selesai");

			drivers.switchTo().defaultContent();
			drivers.switchTo().frame("mainFrame");
			ngDriver.waitForAngularRequestsToFinish();
			System.out.println("Click some data");
			int document = 3 + 2;
			for (int x = 2; x < document; x++) {
				WebElement AllCheckBoxes = drivers
						.findElement(By.xpath("/html/body/form/table[3]/tbody/tr[" + x + "]/td[1]/input"));
				AllCheckBoxes.click();
			}

			System.out.println("Download");
			System.out.println(drivers.getPageSource());
			
			WebElement downloadButtonFinal = drivers.findElement(By.xpath("/html/body/form/table[3]/tbody/tr[28]/td/input[2]"));
//			jsExecutor.executeScript("onDownloadMultiple(this.form);");
			String onclickFunctionName = downloadButtonFinal.getAttribute("onclick").split("(")[0];
			String function  = (String) jsExecutor.executeScript("return " + onclickFunctionName);
			ngDriver.waitForAngularRequestsToFinish();
			Thread.sleep(5000);
////			Don't Erase
////			Log Out
			System.out.println("Log Out");
			drivers.switchTo().defaultContent();
			drivers.switchTo().frame("topFrame");
			WebElement logOutButton = drivers
					.findElement(By.xpath("/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td[4]/a/div"));
			logOutButton.click();
			System.out.println(System.getProperty("user.dir"));
			return response;
			
			
//			/html/body/form/table[3]/tbody/tr[28]/td/input[2]
//			Download Path
		} catch (Exception e) {
			System.out.println("Log Out");
			drivers.switchTo().defaultContent();
			drivers.switchTo().frame("topFrame");
			WebElement logOutButton = drivers
					.findElement(By.xpath("/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr/td[4]/a/div"));
			logOutButton.click();
			return "May Be Another User Still Login In This Web";
		}
	}
}

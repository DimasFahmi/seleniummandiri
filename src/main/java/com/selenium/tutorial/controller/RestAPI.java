package com.selenium.tutorial.controller;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.paulhammant.ngwebdriver.NgWebDriver;

@RestController
public class RestAPI {

	@GetMapping("/")
	public void login() throws InterruptedException {
//		Initiation
		System.setProperty("webdriver.chrome.driver",
				"C:\\CashlezProject\\Selenium Java and Driver\\chromedriver\\chromedriver.exe");

//		Selenium Without Browser
//		ChromeOptions opt = new ChromeOptions();
//		opt.addArguments("--headless", "--window-size=1920,1200","--ignore-certificate-errors");
		WebDriver drivers = new ChromeDriver();
		drivers.manage().window().maximize();

		JavascriptExecutor js = (JavascriptExecutor) drivers;
		NgWebDriver ngDriver = new NgWebDriver(js);

//	        Login Code
		drivers.get("https://staging.cashlez.com:9443/access-portal-v2/#!/login");
		ngDriver.waitForAngularRequestsToFinish();
		WebElement loginUsername = drivers.findElement(By.id("username"));
		loginUsername.sendKeys("merchant");
		WebElement loginPassword = drivers.findElement(By.id("password"));
		loginPassword.sendKeys("Aiueo@123");
		WebElement loginButton = drivers.findElement(
				By.cssSelector(".md-button.md-primary.md-raised.btn-block.cashlez-submit-login.ng-binding"));
		loginButton.sendKeys(Keys.ENTER);
		ngDriver.waitForAngularRequestsToFinish();
		Thread.sleep(5000);
		System.out.println(drivers.getCurrentUrl());

//		Settlement Menu
		WebElement settlementButton = drivers.findElement(By.xpath("//*[@id=\"settlementDropDown\"]"));
		settlementButton.sendKeys(Keys.ENTER);
		ngDriver.waitForAngularRequestsToFinish();
		Thread.sleep(5000);
		System.out.println(drivers.getCurrentUrl());

//		Settlement Report Menu
		WebElement settlementReportButton = drivers.findElement(By.xpath("//*[@id=\"settlementReportMenu\"]"));
		settlementReportButton.sendKeys(Keys.ENTER);
		ngDriver.waitForAngularRequestsToFinish();

		Thread.sleep(5000);
		System.out.println(drivers.getCurrentUrl());
		System.out.println(drivers.getPageSource());

//		Download File
		WebElement downloadButton = drivers
				.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/div[3]/form/div/div[2]/div[2]/button[1]"));
		downloadButton.click();
		Thread.sleep(5000);
		String scriptToExecute = "var performance = window.performance || window.mozPerformance || window.msPerformance || window.webkitPerformance || {}; var network = performance.getEntries() || {}; return network;";
		String netData = ((JavascriptExecutor) drivers).executeScript(scriptToExecute).toString();
		System.out.println(netData);
		System.out.println("Settlement Download");

//		Quit Browser
		System.out.println("Prepare to Quit Web Broewser");
//		drivers.quit();
	}

	@GetMapping("/x")
	public void loginx() throws InterruptedException {
//		Initiation
		System.setProperty("webdriver.chrome.driver",
				"C:\\CashlezProject\\Selenium Java and Driver\\chromedriver\\chromedriver.exe");

//		Selenium Without Browser
//		ChromeOptions opt = new ChromeOptions();
//		opt.addArguments("--headless", "--window-size=1920,1200", "--ignore-certificate-errors");
		WebDriver drivers = new ChromeDriver();
		drivers.manage().window().maximize();

		JavascriptExecutor js = (JavascriptExecutor) drivers;
		NgWebDriver ngDriver = new NgWebDriver(js);

//	        Login Code
		drivers.get("https://staging.cashlez.com:9443/access-portal-v2/#!/login");
		ngDriver.waitForAngularRequestsToFinish();
		WebElement loginUsername = drivers.findElement(By.id("username"));
		loginUsername.sendKeys("merchant");
		WebElement loginPassword = drivers.findElement(By.id("password"));
		loginPassword.sendKeys("Aiueo@123");
		WebElement loginButton = drivers.findElement(
				By.cssSelector(".md-button.md-primary.md-raised.btn-block.cashlez-submit-login.ng-binding"));
		loginButton.sendKeys(Keys.ENTER);
		ngDriver.waitForAngularRequestsToFinish();
		Thread.sleep(2000);
		System.out.println(drivers.getCurrentUrl());

//		Settlement Menu
		WebElement settlementButton = drivers.findElement(By.xpath("//*[@id=\"settlementDropDown\"]"));
		settlementButton.sendKeys(Keys.ENTER);
		ngDriver.waitForAngularRequestsToFinish();
		Thread.sleep(2000);
		System.out.println(drivers.getCurrentUrl());

//		Settlement Report Menu
		WebElement settlementReportButton = drivers.findElement(By.xpath("//*[@id=\"settlementReportMenu\"]"));
		settlementReportButton.sendKeys(Keys.ENTER);
		ngDriver.waitForAngularRequestsToFinish();

		Thread.sleep(2000);
		System.out.println(drivers.getCurrentUrl());
//		System.out.println(drivers.getPageSource());

//		Download File
		WebElement downloadButton = drivers
				.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/div[3]/form/div/div[2]/div[2]/button[1]"));
		downloadButton.click();
		openNewTab(drivers, drivers.getCurrentUrl() + ".js?v=2.3.11", 1);
//		WebDriverWait wait = new WebDriverWait(drivers, Duration.ofSeconds(5));
//		wait.until(ExpectedConditions.elementToBeClickable(
//				By.xpath("//*[@id=\\\"content\\\"]/div[1]/div/div[3]/form/div/div[2]/div[2]/button[1]"))).click();
		Thread.sleep(2000);
		System.out.println(drivers.getCurrentUrl());
//		WebDriverWait wait = new WebDriverWait(drivers, Duration.ofSeconds(5));
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\\\"content\\\"]/div[1]/div/div[3]/form/div/div[2]/div[2]/button[1]")));
//		drivers.findElement(By.xpath("//*[@id=\\\"content\\\"]/div[1]/div/div[3]/form/div/div[2]/div[2]/button[1]")).click();
		Thread.sleep(2000);
		
//	    String scriptToExecute = "var performance = window.performance || window.mozPerformance || window.msPerformance || window.webkitPerformance || {}; var network = performance.getEntries() || {}; return network;";
//	    String netData = ((JavascriptExecutor) drivers).executeScript(scriptToExecute).toString();
//	    System.out.println(netData);
		System.out.println(drivers.getCurrentUrl());
		System.out.println("Settlement Download");


//		//Javascript Executor
//	      JavascriptExecutor j = (JavascriptExecutor) drivers;
//	      String s = (String) j.executeScript("return document.body.innerHTML");
//	      System.out.println(s);

//		Quit Browser
		System.out.println("Prepare to Quit Web Broewser");
		drivers.quit();
	}
	
	public static void openNewTab(WebDriver webDriver, String url, int position) {
        ((JavascriptExecutor) webDriver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<>(webDriver.getWindowHandles());
        System.out.println("tabs : " + tabs.size() + " >position: " + position + " >\t" + url);
        webDriver.switchTo().window(tabs.get(position));
        webDriver.get(url);
    }

}

package com.selenium.tutorial.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.ReceivedDateTerm;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selenium.tutorial.util.JavaxLibraryMethod;

@Service
public class MailService {
	
	@Autowired
	JavaxLibraryMethod javaxLibraryMethod;

	public void check(String host, String mailStoreType, String username, String password) {
		try {
			// create properties
			Properties properties = new Properties();
			properties.put("mail.imap.host", host);
			properties.put("mail.imap.port", "993");
			properties.put("mail.imap.starttls.enable", "true");
			properties.put("mail.imap.ssl.trust", host);

			Session emailSession = Session.getDefaultInstance(properties);

			// create the imap store object and connect to the imap server
			Store store = emailSession.getStore("imaps");

			store.connect(host, username, password);

			// create the inbox object and open it
			Folder inbox = store.getFolder("Inbox");
			inbox.open(Folder.READ_WRITE);

			// retrieve the messages from the folder in an array and print it
//			Message[] messages = inbox.search(new FlagTerm(new Flags(Flag.SEEN), false));

			Calendar cal = Calendar.getInstance();
			System.out.println("The Current Date is:" + cal.getTime());
			
			LocalDate dateNow = LocalDate.now(); // Create a date object
//		    System.out.println(myObj); // Display the current date

//			cal.add(Calendar.DAY_OF_MONTH, -1);

			// We would get the bounce mails received yesterday

			ReceivedDateTerm term = new ReceivedDateTerm(ComparisonTerm.EQ, cal.getTime());

			Message[] messagess = inbox.search(term);

			System.out.println("messages.length---" + messagess.length);

			for (int i = 0, n = messagess.length; i < n; i++) {
				Message message = messagess[i];
				message.setFlag(Flag.SEEN, true);
//				System.out.println("Subject: " + message.getSubject());
				if (message.getSubject().contains("Settlement from OVO")) {
					System.out.println("---------------------------------");
					System.out.println("Email Number " + (i + 1));
					System.out.println("Subject: " + message.getSubject());
					System.out.println("From: " + message.getFrom()[0]);
					System.out.println("Text: " + javaxLibraryMethod.getTextFromMessage(message));
					String file = javaxLibraryMethod.getNameFiledDwnloadAttachments(message);
					javaxLibraryMethod.downloadAttachments(message, dateNow.toString(), file);
					String nameUnZipFIle = javaxLibraryMethod.unZipFile(dateNow.toString(), file);
					System.out.println(nameUnZipFIle);
					javaxLibraryMethod.getDataFromWxcel(nameUnZipFIle);
				}
			}

			inbox.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

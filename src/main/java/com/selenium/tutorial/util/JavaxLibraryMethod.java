package com.selenium.tutorial.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

@Service
public class JavaxLibraryMethod {

	public String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	public String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + Jsoup.parse(html).text();
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}

	public String getNameFiledDwnloadAttachments(Message message) throws IOException, MessagingException {
		Multipart multiPart = (Multipart) message.getContent();
		int numberOfParts = multiPart.getCount();
		String file = "";
		for (int partCount = 0; partCount < numberOfParts; partCount++) {
			MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
			if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
				file = part.getFileName();
//				part.saveFile("C:\\Users\\CZ-User\\Desktop\\FileDownload\\ZipFile" + File.separator + timestamp + "__" + file);
//				downloadedAttachments.add(file);
			}
		}
		return file;
	}

	public List<String> downloadAttachments(Message message, String timestamp, String file)
			throws IOException, MessagingException {
		List<String> downloadedAttachments = new ArrayList<String>();
		Multipart multiPart = (Multipart) message.getContent();
		int numberOfParts = multiPart.getCount();
		for (int partCount = 0; partCount < numberOfParts; partCount++) {
			MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
			if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
//				file = part.getFileName();
				part.saveFile("C:\\Users\\CZ-User\\Desktop\\FileDownload\\ZipFile" + File.separator + timestamp + "__"
						+ file);
				downloadedAttachments.add(file);
				System.out.println("File Downloaded");
			}
		}
		return downloadedAttachments;
	}

//	UnZip File
	public String unZipFile(String timestamp, String file) throws IOException {
		String fileZip = "C:\\Users\\CZ-User\\Desktop\\FileDownload\\ZipFile" + File.separator + timestamp + "__"
				+ file;
		File destDir = new File("C:\\Users\\CZ-User\\Desktop\\FileDownload\\UnZipFile");
		String fileName = "";
				
		byte[] buffer = new byte[1024];
		ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
		ZipEntry zipEntry = zis.getNextEntry();
		while (zipEntry != null) {
			File newFile = newFile(destDir, zipEntry);
			if (zipEntry.isDirectory()) {
				if (!newFile.isDirectory() && !newFile.mkdirs()) {
					throw new IOException("Failed to create directory " + newFile);
				}
			} else {
				// fix for Windows-created archives
				File parent = newFile.getParentFile();
				if (!parent.isDirectory() && !parent.mkdirs()) {
					throw new IOException("Failed to create directory " + parent);
				}

				// write file content
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
			}
			fileName = zipEntry.getName();
			zipEntry = zis.getNextEntry();
		}

		zis.closeEntry();
		zis.close();
		return fileName;
	}

	public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
		File destFile = new File(destinationDir, zipEntry.getName());

		String destDirPath = destinationDir.getCanonicalPath();
		String destFilePath = destFile.getCanonicalPath();

		if (!destFilePath.startsWith(destDirPath + File.separator)) {
			throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
		}

		return destFile;
	}

	public void getDataFromWxcel(String fileName) throws Exception {
		FileInputStream file = new FileInputStream(new File("C:\\Users\\CZ-User\\Desktop\\FileDownload\\UnZipFile\\" + fileName));
		Workbook workbook = new XSSFWorkbook(file);
		Sheet sheet = workbook.getSheetAt(0);

		Map<Integer, List<String>> data = new HashMap<>();
		int i = 0;
		for (Row row : sheet) {
		    data.put(i, new ArrayList<String>());
		    for (Cell cell : row) {
		    	System.out.print(cell.toString() + " ");
//		        switch (cell.getCellType()) {
//		        case Cell.CELL_TYPE_NUMERIC:
//                    System.out.print(cell.getNumericCellValue() + "t");
//                    break;
//                case STRING:
//                    System.out.print(cell.getStringCellValue() + "t");
//                    break;
//		        }
		    }
		    System.out.println();
		    i++;
		}
	}
}
